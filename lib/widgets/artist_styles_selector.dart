import 'package:fixpic/storage/_storage.dart';
import 'package:flutter/widgets.dart';

typedef ArtStyleSelectionChanged = Function(ArtStyle style, bool selected);

class ArtistStylesController extends ChangeNotifier {
  ArtistStylesController({required this.styles, Map<ArtStyle, bool>? selected})
      : _preset = selected;

  final Set<ArtStyle> styles;

  Map<ArtStyle, bool>? _preset;
  Map<ArtStyle, bool>? _selected;
  Map<ArtStyle, bool> get selected => _selected ??= _emptySelection(styles);

  Map<ArtStyle, bool> _emptySelection(Set<ArtStyle> styles) {
    final result = <ArtStyle, bool>{};

    styles.forEach((s) => result[s] = false);
    _preset?.forEach((s, value) => result[s] = value);

    return result;
  }
  // styles.map<ArtStyle, bool>(
  //     (key, value) => MapEntry<ArtStyle, bool>(key, false));

  bool _isSelected(ArtStyle style) => selected[style] ??= false;

  void _changeSelection({required ArtStyle style}) {
    selected[style] = !_isSelected(style);
    notifyListeners();
  }
}

class ArtistStylesSelector extends StatelessWidget {
  const ArtistStylesSelector({
    required this.controller,
    required this.onSelectionChanged,
  }) : super();

  final ArtistStylesController controller;
  final ArtStyleSelectionChanged onSelectionChanged;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
