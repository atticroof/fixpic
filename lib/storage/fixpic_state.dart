import 'package:fixpic/pages/_pages.dart';
import 'package:fixpic/pages/artist/artist_change_info.dart';
import 'package:fixpic/pages/artist/artist_change_styles.dart';

import '_storage.dart';

abstract class FixpicState
    implements
        FixpicAppState,
        ArtistAuthCodeState,
        ArtistAuthPhoneState,
        ArtistChangeInfoState,
        ArtistChangeStylesState,
        ArtistHomeState,
        ArtistDetailsState,
        RandomSixState,
        UserHomeState {
  static FixpicState initial({required Set<ArtStyle> styles}) =>
      FixpicStateImpl(styles: styles);

  FixpicState copyWith({
    ArtistInfo? artistInfo,
    String? errorText,
    Map<num, ArtistInfo>? randomSix,
    bool? loading,
    bool? showSendCode,
    Set<ArtStyle>? styles,
    ArtStyle? artStyle,
  }) =>
      FixpicStateImpl(
        artistInfo: artistInfo ?? this.artistInfo,
        errorText: errorText ?? this.errorText,
        randomSix: randomSix ?? this.randomSix,
        loading: loading ?? this.loading,
        showSendCode: showSendCode ?? this.showSendCode,
        styles: styles ?? this.styles,
        styleInfo: artStyle ?? this.styleInfo,
      );

  FixpicState copyWithError(String? errorText) => FixpicStateImpl(
      artistInfo: artistInfo,
      errorText: errorText,
      randomSix: randomSix,
      loading: loading,
      showSendCode: showSendCode,
      styles: styles,
      styleInfo: styleInfo);

  FixpicState copyWithStyle(ArtStyle? artStyle) => FixpicStateImpl(
      artistInfo: artistInfo,
      errorText: errorText,
      randomSix: randomSix,
      loading: loading,
      showSendCode: showSendCode,
      styles: styles,
      styleInfo: artStyle);

  FixpicState copyWithArtistInfo(ArtistInfo? artistInfo) => FixpicStateImpl(
      artistInfo: artistInfo,
      errorText: errorText,
      randomSix: randomSix,
      loading: loading,
      showSendCode: showSendCode,
      styles: styles,
      styleInfo: styleInfo);
}

class FixpicStateImpl extends FixpicState {
  FixpicStateImpl({
    this.artistInfo,
    this.errorText,
    this.randomSix,
    this.styleInfo,
    this.loading = false,
    this.showSendCode = false,
    required this.styles,
  });

  @override
  final ArtistInfo? artistInfo;

  @override
  final String? errorText;

  @override
  final bool loading;

  @override
  final Map<num, ArtistInfo>? randomSix;

  @override
  final bool showSendCode;

  @override
  final Set<ArtStyle> styles;

  @override
  ArtStyle? styleInfo;
}
