import '../enums.dart';

class ArtistInfo {
  final num id;
  final bool ready;

  final String username;
  final String account;
  final String avatar;

  final Map<ArtStyle, bool> styles;

  ArtistInfo copyWith({
    String? username,
    String? account,
    String? avatar,
    Map<ArtStyle, bool>? styles,
  }) =>
      ArtistInfo(
        id: id,
        ready: ready,
        username: username ?? this.username,
        account: account ?? this.account,
        avatar: avatar ?? this.avatar,
        styles: styles ?? this.styles,
      );

  ArtistInfo({
    required this.id,
    required this.ready,
    required this.username,
    required this.account,
    required this.avatar,
    this.styles = const <ArtStyle, bool>{},
  });
}
