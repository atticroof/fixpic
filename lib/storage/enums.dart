enum ArtStyle {
  tik_tok,
  stories,
  portrait,
  social_media,
  kids_family,
  head_shot,
  wedding,
  love_story,
  event,
  pregnancy,
  newborn,
  glamour,
  food,
  real_estate,
}

enum AuthStatus { unauth, user, artist }
