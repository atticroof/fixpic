import 'package:apd_udf/apd_udf.dart';

import 'enums.dart';
import 'fixpic_state.dart';
import 'objects/_objects.dart';

class Storage {
  Model<FixpicState>? _model;
  Model<FixpicState> get model =>
      _model ??= Model<FixpicState>(FixpicState.initial(styles: styles));

  Set<ArtStyle>? _styles;
  Set<ArtStyle> get styles {
    if (_styles == null) {
      _styles = <ArtStyle>{};
      ArtStyle.values.forEach((s) => _styles!.add(s));
    }

    return _styles!;
  }

  Reducer<FixpicState> get hideSendCode =>
      (state) => state.copyWith(showSendCode: false);

  Reducer<FixpicState> get showSendCode =>
      (state) => state.copyWith(showSendCode: true);

  Reducer<FixpicState> showLoading(bool loading) =>
      (state) => state.copyWith(loading: loading);

  Reducer<FixpicState> uploadStyles(Set<ArtStyle> styles) =>
      (state) => state.copyWith(styles: styles);

  Reducer<FixpicState> setArtistInfo(ArtistInfo artistInfo) =>
      (state) => state.copyWithArtistInfo(artistInfo);

  Reducer<FixpicState> setStyle(ArtStyle style) =>
      (state) => state.copyWithStyle(style);

  Reducer<FixpicState> setRandomSix(Map<num, ArtistInfo> randomSix) =>
      (state) => state.copyWith(randomSix: randomSix);

  Reducer<FixpicState> showError([String? error]) =>
      (state) => state.copyWithError(error);
}
