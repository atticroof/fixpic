import 'package:fixpic/storage/_storage.dart';

abstract class Network {
  CommonAPI get commonApi;
  UserAPI get userApi;
  ArtistAPI get artistApi;
}

abstract class CommonAPI {
  Future<AuthStatus> getAuthStatus();

  Future signIn();
  Future signOut();

  Future<String?> artistSignIn(String email, String password);
  Future<String?> artistSignUp(String email, String password);
}

abstract class UserAPI {
  Future<Map<num, ArtistInfo>> getRandom(ArtStyle style, {int quantity = 6});
}

abstract class ArtistAPI {
  Future<ArtistInfo> getInfo();
  Future submitInfo(String username, String account, String avatar);
  Future submitStyles(Map<ArtStyle, bool> styles);
}
