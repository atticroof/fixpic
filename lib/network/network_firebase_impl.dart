import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixpic/storage/_storage.dart';

import 'network.dart';

class NetworkFirebaseImpl implements Network, UserAPI, CommonAPI, ArtistAPI {
  final _auth = FirebaseAuth.instance;
  final _firestore = FirebaseFirestore.instance;

  @override
  ArtistAPI get artistApi => this;

  @override
  CommonAPI get commonApi => this;

  @override
  UserAPI get userApi => this;

  @override
  Future<String?> artistSignUp(String email, String password) {
    // TODO: implement artistConfirm
    throw UnimplementedError();
  }

  @override
  Future<String?> artistSignIn(String email, String password) {
    final completer = Completer<String?>();

    _auth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) => completer.complete())
        .onError<FirebaseAuthException>(
            (error, stackTrace) => completer.complete(error.code));

    return completer.future;
  }

  @override
  Future<AuthStatus> getAuthStatus() async {
    await Future.delayed(Duration(milliseconds: 222));

    return AuthStatus.unauth;
  }

  @override
  Future<ArtistInfo> getInfo() {
    // TODO: implement getInfo
    throw UnimplementedError();
  }

  @override
  Future<Map<num, ArtistInfo>> getRandom(ArtStyle style, {int quantity = 6}) {
    // TODO: implement getRandom
    throw UnimplementedError();
  }

  @override
  Future signOut() => _auth.signOut();

  @override
  Future submitInfo(String username, String account, String avatar) {
    // TODO: implement submitInfo
    throw UnimplementedError();
  }

  @override
  Future submitStyles(Map<ArtStyle, bool> styles) {
    // TODO: implement submitStyles
    throw UnimplementedError();
  }

  @override
  Future signIn() {
    // TODO: implement userLogin
    throw UnimplementedError();
  }
}
