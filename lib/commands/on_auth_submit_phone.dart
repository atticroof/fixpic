import 'fixpic_command.dart';

class OnAuthSubmitPhone extends FixpicCommand {
  OnAuthSubmitPhone({required this.phone});

  final String phone;

  bool get ready => $.storage.model.value.artistInfo?.ready ?? false;
  FixpicCommand get forwardCommand =>
      ready ? $.commands.showArtistHome(!ready) : $.commands.showArtistSetup;

  _reLogin(String phone) async {
    loading = true;
    final error = await $.network.commonApi.artistSignUp(phone, '');
    loading = false;

    if (error == null) {
      $.commands.showArtistAuthCode(phone: phone).execute();
      return;
    }

    blinkError(error);
  }

  _confirm(String code) async {
    loading = true;
    final error = await $.network.commonApi.artistSignUp(code, '');
    $.storage.model.reduce($.storage.setArtistInfo(
      await $.network.artistApi.getInfo(),
    ));
    loading = false;

    if (error == null) {
      forwardCommand.execute();
      return;
    }

    blinkError(error);
  }

  @override
  Future execute() async {
    $.storage.model.reduce($.storage.hideSendCode);
    Future.delayed(Duration(seconds: 2))
        .then((_) => $.storage.model.reduce($.storage.showSendCode));

    $.keys.generalKey.currentState?.push($.routes.regular($.pages
        .artistAuthCode(
            onCodeChanged: (code) => _confirm(code),
            onSendCode: () => _reLogin(phone))));
  }
}
