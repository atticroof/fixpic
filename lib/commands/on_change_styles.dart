import 'package:fixpic/storage/_storage.dart';

import 'fixpic_command.dart';

class OnChangeStyles extends FixpicCommand {
  _submitStyles(Map<ArtStyle, bool> map) async {
    loading = true;
    $.storage.model.reduce($.storage.setArtistInfo(
      await $.network.artistApi.getInfo(),
    ));
    loading = false;

    $.keys.generalKey.currentState?.pop();
  }

  @override
  Future execute() async {
    $.keys.generalKey.currentState?.push($.routes.regular($.pages
        .artistChangeStyles(
            onStylesChanged: (Map<ArtStyle, bool> map) => _submitStyles(map))));
  }
}
