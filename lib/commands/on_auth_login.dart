import 'fixpic_command.dart';

class OnAuthLogin extends FixpicCommand {
  _login(String phone) async {
    loading = true;
    final error = await $.network.commonApi.artistSignIn('', '');

    if (error == null) {
      $.commands.showArtistAuthCode(phone: phone).execute();
      loading = false;
      return;
    } else
      blinkError(error);
  }

  @override
  Future execute() async {
    $.keys.generalKey.currentState?.push($.routes.regular($.pages.artistAuth(
      onPhoneChanged: (phone) => _login(phone),
    )));
  }
}
