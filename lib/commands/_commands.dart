export 'startup.dart';

export 'show_welcome.dart';
export 'show_user_home.dart';
export 'show_random_six.dart';
export 'show_artist_details.dart';

export 'show_artist_home.dart';
export 'show_artist_setup.dart';

export 'on_logout.dart';

export 'on_user_login.dart';

export 'on_change_info.dart';
export 'on_change_styles.dart';

export 'on_auth_login.dart';
export 'on_auth_submit_phone.dart';
