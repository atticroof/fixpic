import 'fixpic_command.dart';

class OnUserLogin extends FixpicCommand {
  @override
  Future execute() async {
    loading = true;
    await $.network.commonApi.signIn();
    $.commands.showUserHome.execute();
    loading = false;
  }
}
