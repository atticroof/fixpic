import 'package:fixpic/pages/_pages.dart';

import 'fixpic_command.dart';

class ShowArtistSetup extends FixpicCommand {
  _submitInfo(String username, String account, String avatar) async {
    await $.network.artistApi.submitInfo(username, account, avatar);
    $.commands.showArtistHome(true).execute();
  }

  @override
  Future execute() async {
    $.keys.generalKey.currentState?.pushAndRemoveUntil(
        $.routes.regular(ArtistAuthInfo(
            onBaseInfoChanged: (username, account, avatar) =>
                _submitInfo(username, account, avatar))),
        (route) => false);
  }
}
