import 'package:fixpic/storage/_storage.dart';

import 'fixpic_command.dart';

class ShowRandomSix extends FixpicCommand {
  ArtStyle get style => $.storage.model.value.styleInfo!;

  Future _update() async => $.storage.model.reduce($.storage.setRandomSix(
        await $.network.userApi.getRandom(style),
      ));

  @override
  Future execute() async {
    await _update();

    $.keys.generalKey.currentState?.push($.routes.regular(
      $.pages.randomSix(
        onRefresh: () => _update(),
        onArtistSelect: (info) {
          $.storage.model.reduce($.storage.setArtistInfo(info));
          $.commands.showArtistDetails.execute();
        },
      ),
    ));
  }
}
