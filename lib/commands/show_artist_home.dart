import 'fixpic_command.dart';

class ShowArtistHome extends FixpicCommand {
  ShowArtistHome({required this.showSelectStyles});

  final bool showSelectStyles;

  @override
  Future execute() async {
    $.keys.generalKey.currentState?.pushAndRemoveUntil(
      $.routes.regular($.pages.artistHome(
        onExit: () => $.commands.onLogout.execute(),
        onChangeStyles: () => $.commands.onChangeStyles.execute(),
        onChangeInfo: () => $.commands.onChangeInfo.execute(),
      )),
      (route) => false,
    );

    if (showSelectStyles)
      Future.delayed(Duration(milliseconds: 500))
          .then((_) => $.commands.onChangeStyles.execute());
  }
}
