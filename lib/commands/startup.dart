import 'package:fixpic/storage/_storage.dart';

import 'fixpic_command.dart';

class Startup extends FixpicCommand {
  bool get ready => $.storage.model.value.artistInfo?.ready ?? false;
  FixpicCommand get forwardCommand =>
      ready ? $.commands.showArtistHome() : $.commands.showArtistSetup;

  @override
  Future execute() async {
    switch (await $.network.commonApi.getAuthStatus()) {
      case AuthStatus.unauth:
        $.commands.showWelcome.execute();
        break;
      case AuthStatus.user:
        $.commands.showUserHome.execute();
        break;
      case AuthStatus.artist:
        $.storage.model.reduce($.storage.setArtistInfo(
          await $.network.artistApi.getInfo(),
        ));

        forwardCommand.execute();

        break;
    }
  }
}
