import 'package:fixpic/pages/_pages.dart';
import 'package:fixpic/storage/_storage.dart';

import 'fixpic_command.dart';

class ShowArtistDetails extends FixpicCommand {
  ArtistInfo get info => $.storage.model.value.artistInfo!;

  @override
  Future execute() async {}
}
