import 'fixpic_command.dart';

class OnLogout extends FixpicCommand {
  @override
  Future execute() async {
    loading = true;
    await $.network.commonApi.signOut();
    $.commands.showWelcome.execute();
    loading = false;
  }
}
