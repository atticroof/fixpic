import 'package:fixpic/facade/fixpic_facade.dart';

abstract class FixpicCommand {
  static FixpicFacade? _$;
  FixpicFacade get $ => _$ ??= FixpicFacade.instant;

  bool get loading => $.storage.model.value.loading;
  set loading(bool value) =>
      $.storage.model.reduce($.storage.showLoading(value));

  void blinkError(String error) {
    $.storage.model.reduce($.storage.showError(error));
    Future.delayed(Duration(seconds: 2))
        .then((_) => $.storage.model.reduce($.storage.showError()));
  }

  Future execute();
}
