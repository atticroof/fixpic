import 'package:fixpic/pages/artist/artist_change_info.dart';

import 'fixpic_command.dart';

class OnChangeInfo extends FixpicCommand {
  _submitInfo(String username, String account, String avatar) async {
    loading = true;
    await $.network.artistApi.submitInfo(username, account, avatar);
    $.storage.model.reduce($.storage.setArtistInfo(
      await $.network.artistApi.getInfo(),
    ));
    loading = false;
    $.keys.generalKey.currentState?.pop();
  }

  @override
  Future execute() async {
    $.keys.generalKey.currentState?.push($.routes.regular(ArtistChangeInfo(
        onBaseInfoChanged: (String username, String account, String avatar) =>
            _submitInfo(username, account, avatar))));
  }
}
