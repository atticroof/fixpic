import 'fixpic_command.dart';

class ShowUserHome extends FixpicCommand {
  @override
  Future execute() async {
    /// попробуем финт ушами внизу,
    /// когда await как параметр у конструктора
    ///
    /// final styles = await $.network.commonApi.getStyles();

    $.keys.generalKey.currentState?.pushAndRemoveUntil(
        $.routes.regular($.pages.userHome(
          onStyleSelected: (style) {
            $.storage.model.reduce($.storage.setStyle(style));
            $.commands.showRandomSix.execute();
          },
          onExit: () => $.commands.onLogout.execute(),
        )),
        (_) => false);
  }
}
