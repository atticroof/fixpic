import 'package:firebase_core/firebase_core.dart';
import 'package:fixpic/facade/fixpic_facade.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  await Firebase.initializeApp();
  final $ = FixpicFacade.instant;

  runApp($.pages.fixpicApp(
    generalKey: $.keys.generalKey,
    home: $.pages.preloader,
  ));

  $.commands.startup.execute();
}
