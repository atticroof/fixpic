import 'package:fixpic/storage/_storage.dart';

import 'fixpic_coordinator.dart';

class UserCoordinator extends FixpicCoordinator {
  Future _update(ArtStyle style) async =>
      $.storage.model.reduce($.storage.setRandomSix(
        await $.network.userApi.getRandom(style),
      ));

  void _showArtistDetails(ArtistInfo info) {
    $.storage.model.reduce($.storage.setArtistInfo(info));
    navigator?.push($.routes.regular(
        $.pages.userArtistDetails(onTapContacts: () => print(info.account))));
  }

  void _refreshRandomSix(ArtStyle style) async {
    loading = true;
    await _update(style);
    loading = false;
  }

  void _showRandomSix(ArtStyle style) async {
    loading = true;
    await _update(style);

    navigator?.push($.routes.regular($.pages.randomSix(
      onArtistSelect: _showArtistDetails,
      onRefresh: () => _refreshRandomSix(style),
    )));

    loading = false;
  }

  @override
  void run() {
    navigator?.pushAndRemoveUntil(
        $.routes.regular($.pages.userHome(
          onStyleSelected: _showRandomSix,
          onExit: () => $.commands.onLogout.execute(),
        )),
        (_) => false);
  }
}
