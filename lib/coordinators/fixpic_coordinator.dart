import 'package:fixpic/facade/fixpic_facade.dart';
import 'package:flutter/widgets.dart';

import 'base_coordinator.dart';

abstract class FixpicCoordinator extends BaseCoordinator {
  // FixpicCoordinator(this.facade);
  //
  // final FixpicFacade facade;

  late final FixpicFacade $ = FixpicFacade.instant;

  bool get loading => $.storage.model.value.loading;
  set loading(bool value) =>
      $.storage.model.reduce($.storage.showLoading(value));

  void blinkError(String error) {
    $.storage.model.reduce($.storage.showError(error));
    Future.delayed(Duration(seconds: 2))
        .then((_) => $.storage.model.reduce($.storage.showError()));
  }

  NavigatorState? get navigator => $.keys.generalKey.currentState;
}
