import 'package:flutter/foundation.dart';

abstract class BaseCoordinator {
  void run();
  late final VoidCallback? onComplete;
}
