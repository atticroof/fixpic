import 'package:fixpic/coordinators/_coordinators.dart';
import 'package:fixpic/storage/_storage.dart';

import 'fixpic_coordinator.dart';

class StartupCoordinator extends FixpicCoordinator {
  void _showUser() async {
    loading = true;
    await $.network.commonApi.signIn();
    $.commands.showUserHome.execute();
    loading = false;
  }

  void _showWelcome() {
    navigator?.pushAndRemoveUntil(
      $.routes.regular($.pages.welcome(
        onUser: _showUser,
        onArtist: () => $.coordinators<AuthCoordinator>().run(),
      )),
      (_) => false,
    );
  }

  @override
  void run() async {
    switch (await $.network.commonApi.getAuthStatus()) {
      case AuthStatus.unauth:
        _showWelcome();
        break;
      case AuthStatus.user:
        $.coordinators<UserCoordinator>().run();
        break;
      case AuthStatus.artist:
        final info = await $.network.artistApi.getInfo();
        $.storage.model.reduce($.storage.setArtistInfo(info));
        $.coordinators<ArtistCoordinator>().run(info.ready);
        break;
    }
  }
}
