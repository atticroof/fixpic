import 'fixpic_coordinator.dart';

class AuthCoordinator extends FixpicCoordinator {
  _login(String phone) async {
    loading = true;
    final error = await $.network.commonApi.artistSignIn('', '');

    if (error == null) {
      $.commands.showArtistAuthCode(phone: phone).execute();
      loading = false;
      return;
    } else
      blinkError(error);
  }

  @override
  void run() {
    navigator?.push($.routes.regular($.pages.artistAuth(
      onPhoneChanged: (phone) => _login(phone),
    )));
  }
}
