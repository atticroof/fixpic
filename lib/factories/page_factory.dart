import 'package:fixpic/pages/_pages.dart';
import 'package:fixpic/pages/artist/artist_change_styles.dart';
import 'package:fixpic/storage/_storage.dart';
import 'package:flutter/widgets.dart';

class PageFactory {
  Widget fixpicApp({
    required GlobalKey<NavigatorState> generalKey,
    required Widget home,
  }) =>
      FixpicApp(generalKey: generalKey, home: home);

  Widget get preloader => Preloader();

  Widget userHome({
    required Function(ArtStyle) onStyleSelected,
    required VoidCallback onExit,
  }) =>
      UserHome(onStyleSelected: onStyleSelected, onExit: onExit);

  Widget randomSix({
    required VoidCallback onRefresh,
    required ValueChanged<ArtistInfo> onArtistSelect,
  }) =>
      RandomSix(onRefresh: onRefresh, onArtistSelect: onArtistSelect);

  Widget userArtistDetails({required VoidCallback onTapContacts}) =>
      ArtistDetails(onTapContacts: onTapContacts);

  Widget artistHome({
    required VoidCallback onExit,
    required VoidCallback onChangeStyles,
    required VoidCallback onChangeInfo,
  }) =>
      ArtistHome(
        onExit: onExit,
        onChangeStyles: onChangeStyles,
        onChangeInfo: onChangeInfo,
      );

  Widget artistAuthCode({
    required ValueChanged<String> onCodeChanged,
    required VoidCallback onSendCode,
  }) =>
      ArtistAuthCode(onCodeChanged: onCodeChanged, onRetry: onSendCode);

  Widget artistAuth({required ValueChanged<String> onPhoneChanged}) =>
      ArtistAuth(onSignUp: onPhoneChanged);

  Widget artistChangeStyles(
          {required ValueChanged<Map<ArtStyle, bool>> onStylesChanged}) =>
      ArtistChangeStyles(onStylesChanged: onStylesChanged);

  Widget welcome(
          {required VoidCallback onUser, required VoidCallback onArtist}) =>
      Welcome(onUser: onUser, onArtist: onArtist);
}
