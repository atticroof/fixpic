import 'package:fixpic/commands/_commands.dart';

class CommandFactory {
  Startup get startup => Startup();

  ShowWelcome get showWelcome => ShowWelcome();

  ShowArtistHome showArtistHome([bool showSelectStyles = false]) =>
      ShowArtistHome(showSelectStyles: showSelectStyles);
  ShowArtistSetup get showArtistSetup => ShowArtistSetup();

  OnAuthSubmitPhone showArtistAuthCode({required String phone}) =>
      OnAuthSubmitPhone(phone: phone);

  ShowUserHome get showUserHome => ShowUserHome();
  ShowRandomSix get showRandomSix => ShowRandomSix();
  ShowArtistDetails get showArtistDetails => ShowArtistDetails();

  OnUserLogin get onUserLogin => OnUserLogin();
  OnLogout get onLogout => OnLogout();

  OnAuthLogin get onAuthLogin => OnAuthLogin();
  OnChangeStyles get onChangeStyles => OnChangeStyles();
  OnChangeInfo get onChangeInfo => OnChangeInfo();
}
