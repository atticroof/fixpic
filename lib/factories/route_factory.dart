import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RouteFactory {
  Route regular(Widget widget) => CupertinoPageRoute(builder: (_) => widget);
}
