import 'package:fixpic/coordinators/_coordinators.dart';
import 'package:flutter/foundation.dart';

class CoordinatorsFactory {
  final _map = {StartupCoordinator: () => StartupCoordinator()};

  T call<T extends BaseCoordinator>([VoidCallback? onComplete]) {
    assert(_map.keys.contains(T), 'No such coordinator registered');
    return (_map[T] as T)()..onComplete = onComplete;
  }
}
