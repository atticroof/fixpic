import 'package:flutter/widgets.dart';

class NavigatorKeys {
  final generalKey = GlobalKey<NavigatorState>();
  final modalKey = GlobalKey<NavigatorState>();
}
