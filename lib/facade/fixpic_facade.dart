import 'package:fixpic/factories/_factories.dart';
import 'package:fixpic/network/network.dart';
import 'package:fixpic/network/network_firebase_impl.dart';
import 'package:fixpic/storage/_storage.dart';
import 'navigator_keys.dart';

class FixpicFacade {
  FixpicFacade._();
  static final FixpicFacade instant = FixpicFacade._();

  late final coordinators = CoordinatorsFactory();

  final commands = CommandFactory();
  final routes = RouteFactory();
  final keys = NavigatorKeys();
  final storage = Storage();
  final pages = PageFactory();

  final Network network = NetworkFirebaseImpl();
}
