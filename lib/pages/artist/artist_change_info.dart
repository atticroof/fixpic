import 'package:flutter/widgets.dart';

import 'package:fixpic/storage/_storage.dart';

import '../fixpic_page.dart';

abstract class ArtistChangeInfoState {
  ArtistInfo? get artistInfo;
}

class ArtistChangeInfo extends FixpicPage<ArtistChangeInfoState> {
  const ArtistChangeInfo({
    required this.onBaseInfoChanged,
  }) : super();

  final Function(
    String username,
    String account,
    String avatar,
  ) onBaseInfoChanged;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
