import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import 'package:fixpic/storage/_storage.dart';

import '../fixpic_page.dart';

abstract class ArtistChangeStylesState {
  Set<ArtStyle> get styles;
}

class ArtistChangeStyles extends FixpicPage<ArtistChangeStylesState> {
  const ArtistChangeStyles({
    required this.onStylesChanged,
  }) : super();

  final ValueChanged<Map<ArtStyle, bool>> onStylesChanged;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
