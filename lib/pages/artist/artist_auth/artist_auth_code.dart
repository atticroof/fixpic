import 'package:flutter/widgets.dart';

import '../../fixpic_page.dart';

abstract class ArtistAuthCodeState {
  String? get errorText;
  bool get showSendCode;
}

class ArtistAuthCode extends FixpicPage<ArtistAuthCodeState> {
  const ArtistAuthCode({
    required this.onCodeChanged,
    required this.onRetry,
  }) : super();

  final ValueChanged<String> onCodeChanged;
  final VoidCallback onRetry;

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
