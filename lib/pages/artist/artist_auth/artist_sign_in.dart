import 'package:apd_udf/apd_udf.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../fixpic_page.dart';

abstract class ArtistAuthPhoneState {
  String? get errorText;
}

class ArtistAuth extends FixpicPage<ArtistAuthPhoneState> {
  ArtistAuth({
    required this.onSignUp,
  }) : super();

  final ValueChanged<String> onSignUp;

  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.all(24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Subscriber<ArtistAuthPhoneState, String?>(
              selector: (state) {
                return state.errorText;
              },
              listenable: listenable,
              builder: (_, errorText, __) => TextField(
                controller: _controller,
                enabled: errorText == null,
                decoration: InputDecoration(
                  hintText: 'input_phone_hint',
                  labelText: 'input_phone_label',
                  errorText: errorText,
                ),
              ),
            ),
            SizedBox(height: 12),
            ElevatedButton(
                onPressed: () => onSignUp(_controller.text),
                child: Text('submit_phone'))
          ],
        ),
      ));
}
