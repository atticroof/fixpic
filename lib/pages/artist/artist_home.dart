import 'package:flutter/widgets.dart';

import 'package:fixpic/storage/_storage.dart';

import '../fixpic_page.dart';

abstract class ArtistHomeState {
  ArtistInfo? get artistInfo;
  Set<ArtStyle> get styles;
}

class ArtistHome extends FixpicPage<ArtistHomeState> {
  const ArtistHome({
    required this.onExit,
    required this.onChangeStyles,
    required this.onChangeInfo,
  }) : super();

  final VoidCallback onExit;
  final VoidCallback onChangeStyles;
  final VoidCallback onChangeInfo;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
