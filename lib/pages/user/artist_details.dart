import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:fixpic/storage/_storage.dart';

import '../fixpic_page.dart';

abstract class ArtistDetailsState {
  ArtistInfo? get artistInfo;
}

class ArtistDetails extends FixpicPage<ArtistDetailsState> {
  const ArtistDetails({
    required this.onTapContacts,
  }) : super();

  final VoidCallback onTapContacts;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(listenable.value.artistInfo?.username ?? ''),
        ),
        body: Container(),
      );
}
