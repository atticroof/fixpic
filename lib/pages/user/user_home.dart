import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:fixpic/storage/_storage.dart';

import '../fixpic_page.dart';

abstract class UserHomeState {
  Set<ArtStyle> get styles;
}

class UserHome extends FixpicPage<UserHomeState> {
  const UserHome({
    required this.onStyleSelected,
    required this.onExit,
  }) : super();

  final Function(ArtStyle style) onStyleSelected;
  final VoidCallback onExit;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('fixpic'),
          actions: [TextButton(child: Text('exit'), onPressed: onExit)],
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              for (ArtStyle info in listenable.value.styles)
                _StyleItem(
                  onSelect: () => onStyleSelected(info),
                  style: info,
                )
            ],
          ),
        ),
      );
}

class _StyleItem extends StatelessWidget {
  const _StyleItem({required this.onSelect, required this.style}) : super();

  final Function() onSelect;
  final ArtStyle style;

  @override
  Widget build(BuildContext context) => SizedBox(
        height: 40,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: [Text(style.toString())],
        ),
      );
}
