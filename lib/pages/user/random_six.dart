import 'package:apd_udf/apd_udf.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:fixpic/storage/_storage.dart';

import '../fixpic_page.dart';

abstract class RandomSixState {
  Map<num, ArtistInfo>? get randomSix;
  ArtStyle? get styleInfo;
  bool get loading;
}

class RandomSix extends FixpicPage<RandomSixState> {
  const RandomSix({
    required this.onRefresh,
    required this.onArtistSelect,
  }) : super();

  final VoidCallback onRefresh;
  final Function(ArtistInfo info) onArtistSelect;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('fixpic'),
          actions: [TextButton(child: Text('randomize'), onPressed: onRefresh)],
        ),
        body: SingleChildScrollView(
          child: Subscriber<RandomSixState, List<ArtistInfo>>(
            listenable: listenable,
            selector: (state) => (state.randomSix ?? {}).values.toList(),
            builder: (_, list, __) => Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                for (ArtistInfo info in list)
                  _ArtistItem(
                    info: info,
                    onSelect: () => onArtistSelect(info),
                  )
              ],
            ),
          ),
        ),
      );
}

class _ArtistItem extends StatelessWidget {
  const _ArtistItem({required this.info, required this.onSelect}) : super();

  final ArtistInfo info;
  final VoidCallback onSelect;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
