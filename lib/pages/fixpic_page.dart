import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import 'package:fixpic/facade/fixpic_facade.dart';

typedef BaseInfoChanged = Function(
    String username, String account, String avatar);

abstract class FixpicPage<T> extends StatelessWidget {
  static final FixpicFacade _$ = FixpicFacade.instant;

  // ValueListenable<T> get listenable => ValueListenableAdapter<FixpicState, T>(
  //     listenable: _$.storage.model, adapter: (fixpicState) => fixpicState as T);

  ValueListenable<T> get listenable => _$.storage.model as ValueListenable<T>;

  const FixpicPage() : super();
}
