import 'package:fixpic/facade/fixpic_facade.dart';
import 'package:fixpic/pages/unauth/preloader.dart';
import 'package:flutter/material.dart';

import 'package:apd_udf/apd_udf.dart';

import '../fixpic_page.dart';

abstract class FixpicAppState {
  bool get loading;
}

class FixpicApp extends FixpicPage<FixpicAppState> {
  FixpicApp({required this.generalKey, required this.home});

  final GlobalKey<NavigatorState> generalKey;
  final Widget home;

  @override
  Widget build(BuildContext context) =>
      MaterialApp(navigatorKey: generalKey, home: home);
  // Subscriber<FixpicAppState, bool>(
  //       listenable: listenable,
  //       selector: (state) => state.loading,
  //       builder: (context, value, child) => Stack(children: [
  //         Positioned.fill(child: home),
  //         if (value) Positioned.fill(child: ColoredBox(color: Colors.black12))
  //       ]),
  //     ));
}
