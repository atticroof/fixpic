import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../fixpic_page.dart';

class Preloader extends FixpicPage {
  @override
  Widget build(BuildContext context) =>
      Scaffold(body: Center(child: Text('fixpic_loading')));
}
