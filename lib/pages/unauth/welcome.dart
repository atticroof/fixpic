import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../fixpic_page.dart';

class Welcome extends FixpicPage {
  final VoidCallback onUser;
  final VoidCallback onArtist;

  const Welcome({
    required this.onUser,
    required this.onArtist,
  }) : super();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text('fixpic')),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: ElevatedButton(
                onPressed: onUser,
                child: Text('fixpic_user_mode'),
              ),
            ),
            SizedBox(height: 16),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: ElevatedButton(
                onPressed: onArtist,
                child: Text('fixpic_artist_mode'),
              ),
            ),
          ],
        ),
      );
}
